package com.citi.training.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJavaAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJavaAssessmentApplication.class, args);
	}

}

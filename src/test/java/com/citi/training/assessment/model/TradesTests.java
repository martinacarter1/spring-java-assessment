package com.citi.training.assessment.model;

import org.junit.Test;

public class TradesTests {

	@Test
	public void test_Trade_fullConstructor() {
		Trade testTrade = new Trade(0, "AAPL", 200.00, 1);
		assert (testTrade.getId() == 0);
		assert (testTrade.getStock().equals("AAPL"));
		assert (testTrade.getPrice() == 200.00);
		assert (testTrade.getVolume() == 1);
	}

}

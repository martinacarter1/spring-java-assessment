package com.citi.training.assessment.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.assessment.dao.TradeDao;
import com.citi.training.assessment.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeServiceTests {

	@Autowired
	private TradeService tradeService;
	
	@MockBean
	private TradeDao mockTradeDao;

	
	@Test
	public void test_createRuns() {
		int newID = 1;
		Trade someTrade = new Trade(newID,"BABA",2.00,3);
		
		when(mockTradeDao.create(any(Trade.class))).thenReturn(newID);
		int createdID = tradeService.create(new Trade(newID,"BABA", 2.00,3));
		verify(mockTradeDao).create(someTrade);
		assertEquals(newID,createdID);
	}
	@Test
	public void test_deleteRuns() {
		int id= 2;
		//when(mockTradeDao.deleteById(id)).then
		tradeService.deleteById(id);
		verify(mockTradeDao).deleteById(id);
	}
	
	@Test
	public void test_findByID() {
		int testID = 3;
//mockTradeDao.create(new Trade(testID, "IBM", 400.99,20));
		Trade testTrade = new Trade(testID, "IBM", 400.99,20);
		
		Trade returnedTrade = tradeService.findById(testID);
		
		assertEquals(testTrade,returnedTrade);
		
	}
	@Test
	public void test_findAll() {
		Trade testTrade = new Trade(3,"NFLX",1.00,1);
		List<Trade> trades;
		//trades.add(testTrade);
		
	}

}




